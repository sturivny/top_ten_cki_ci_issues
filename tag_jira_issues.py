import csv
import re

from src.tools import get_env_variables
from src.jira_methods import get_jira_object, get_jira_issue_id_from_link, jira_get_issue_details, update_label
from src.datawarehouse_methods import get_issues

# List of environment variables to check, all in uppercase
env_variables = [
    'JIRA_USER',
    'JIRA_SERVER',
    'JIRA_TOKEN_AUTH',
    'DATAWAREHOUSE_ENDPOINT',
    'DATAWAREHOUSE_TOKEN'
]


def extract_bracket_content(issue_description):
    # Find all occurrences of text within square brackets
    tags = []
    contents = re.findall(r'\[([^\[\]]+)\]', issue_description)
    for content in contents:
        tags.append(content)
    return tags


def write_dw_jira_tags_to_csv( dw_issue_data, jira_issue_data, filename='tag_issues_output.csv'):
    """
    Save data to csv file
    """

    dw_tags = extract_bracket_content(dw_issue_data['description'])

    # Adjusting fieldnames to match the desired output
    fieldnames = ['jira_issue_name', 'jira_Link', 'jira_created_on', 'jira_priority', 'jira_status', 'jira_component', 'jira_labels',
                  'affects_versions', 'dw_id', 'dw_tags', 'dw_description', 'dw_ticket_url', 'dw_first_seen', 'dw_last_seen', 'dw_kind_tag']

    # Writing data to CSV file
    with open(filename, 'a', newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        # writer.writeheader()

        writer.writerow({'jira_issue_name': jira_issue_data['Issue Name'], 
                         'jira_Link': jira_issue_data['Link'], 
                         'jira_created_on': jira_issue_data['Created_On'], 
                         'jira_priority': jira_issue_data['Priority'], 
                         'jira_status': jira_issue_data['Status'], 
                         'jira_component': jira_issue_data['Component'],
                         'jira_labels': jira_issue_data['Labels'],
                         'affects_versions': jira_issue_data['Affects_Versions'],
                         'dw_id': dw_issue_data['id'], 
                         'dw_tags': dw_tags,
                         'dw_description': dw_issue_data['description'], 
                         'dw_ticket_url': dw_issue_data['ticket_url'], 
                         'dw_first_seen': dw_issue_data['first_seen'], 
                         'dw_last_seen': dw_issue_data['last_seen'], 
                         'dw_kind_tag':dw_issue_data['kind']['tag']})

    print(f"CSV file saved at: {filename}")


def tag_jira_issues(offset):
    "Tag jira issues"
    # Get the environment variables and their values
    env_values = get_env_variables(env_variables)
    jira_obj = get_jira_object(env_values['JIRA_SERVER'], env_values['JIRA_TOKEN_AUTH'])

    # Get all last top X issues
    datawarehouse_endpoint = f"{env_values['DATAWAREHOUSE_ENDPOINT']}/api/1/issue/top/?offset={offset}"
    issues = get_issues(endpoint=datawarehouse_endpoint, datawarehouse_token=env_values['DATAWAREHOUSE_TOKEN'])
    print(f'issues: {len(issues)}')
    for issue in issues:
        # Check that DW issues is not resolved
        if not issue['resolved']:
            # bracket_content = extract_bracket_content(issue['description'])
            
            # Check for only RH issues
            if "issues.redhat.com/browse" in issue['ticket_url']:
            
                jira_issue_id = get_jira_issue_id_from_link(issue['ticket_url'])
                # print(f"===== {issue['ticket_url']}       {jira_issue_id}")
                jira_issues_details = jira_get_issue_details(env_values['JIRA_SERVER'], jira_obj, jira_issue_id)

                # +++ Update Jira labels +++
                # labels = update_label(jira_obj, rhel_issue_id=jira_issue_id, new_label="cki-datawarehouse")

                # jira_issues_details['Labels'] = str(labels)
                # jira_issues_details.update({'Labels': labels})

                # print(f" ===   LABELS {labels} ===")
                # Check for not closed issues
                try:
                    if jira_issues_details['Status'] != 'Closed':

                        print(f"{issue['id']}  {issue['ticket_url']:10}  {jira_issues_details['Status']} {jira_issues_details['component']}")
                        write_dw_jira_tags_to_csv(dw_issue_data=issue, jira_issue_data=jira_issues_details)
                except:
                    continue


if __name__ == '__main__':
    offset = 0
    page = 0
while True:
    print(f"Page: {page}")
    tag_jira_issues(offset)
    offset += 30
    page +=1
    