import requests


def get_all_dw_issues(datawarehouse_token, datawarehouse_endpoint):
    """
    For Gitlab methods
    :param datawarehouse_token:
    :param datawarehouse_endpoint:
    :return:
    """
    headers = {
        'Authorization': f"Token {datawarehouse_token}"
    }
    # Get all last top X issues
    r = requests.get(datawarehouse_endpoint, verify=False, headers=headers)
    resp = r.json()
    return resp['results']


def get_links_from_dw_issues(dw_issues):
    """
    For Gitlab methods

    :param dw_issues:
    :return:
    """
    # Get all links
    all_issues_links = []
    for issue in dw_issues:
        all_issues_links.append(issue['ticket_url'])
    return all_issues_links


def get_issues(endpoint, datawarehouse_token):
    """
    Get DW issues

    :param endpoint: DW endpoint
    :param headers: headers
    :return: list of issues
    """
    headers = {
        'Authorization': f"Token {datawarehouse_token}"
    }
    r = requests.get(endpoint, verify=False, headers=headers)
    resp = r.json()

    issues = resp['results']
    # print("All issues:", issues)

    return issues


def get_issues_raw(endpoint, datawarehouse_token):
    """
    Get DW issues

    :param endpoint: DW endpoint
    :param headers: headers
    :return: list of issues
    """
    headers = {
        'Authorization': f"Token {datawarehouse_token}"
    }
    r = requests.get(endpoint, verify=False, headers=headers)
    return  r.json()


def get_dw_last_x_issues_links(issues):

    # Get all links
    all_issues_links = []
    for issue in issues:
        all_issues_links.append(issue['ticket_url'])
    return all_issues_links
