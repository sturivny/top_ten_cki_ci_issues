

def extract_rhel_bug_ids_from_url(url_list):
    """
    Parse URL to ge bug_id
    :param url_list: list of URLS
    :return: bug_ids
    """
    bug_ids = []
    for url in url_list:
        # Splitting the URL to get the last part which is the bug ID
        parts = url.split('/')
        if parts:
            bug_id = parts[-1]  # The bug ID is the last part of the URL
            bug_ids.append(bug_id)
    return bug_ids
