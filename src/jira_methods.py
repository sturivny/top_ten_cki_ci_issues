import jira

from termcolor import colored

from src.tools import trim_issue_titles


def get_jira_object(jira_server, jira_token_auth):
    """
    Getting Jira object

    :param jira_server: string, secret
    :param jira_token_auth: string, secret
    :return:
    """
    jira_options = {'server': jira_server, 'sslverify': False}
    return jira.JIRA(options=jira_options, token_auth=jira_token_auth)


def get_jira_issue_id_from_link(jira_issue_link):
    "Gets Jira issues ID from the issue link"
    return jira_issue_link.split('/')[-1]


def jira_get_issue_details(jira_server, jira_obj, issue_id):
    """
    Get Jira issue details
    :param issue_id: ID of an issue
    :return: dict with issue details
    """
    try:
        issue = jira_obj.issue(issue_id)
    except Exception as e:
        print(f"No issue object found {str(e)}")
        return {
            "issue_name": '',
            "link": '',
            "created_on": '',
            "priority": '',
            "status": '',
            "assignee"
            "component": '',
            "affects_versions": [],
            "labels": []
        }
    try:
        issue_assignee = issue.fields.assignee.displayName
    except Exception as e:
        print(f"No issue_assignee found {str(e)}")
        issue_assignee = ''

    try:
        issue_name = issue.fields.summary
    
    except Exception as e:
        print(f"No issue_name found {str(e)}")
        issue_name = ''

    try:
        issue_link = f"{jira_server}/browse/{issue.key}"
    except Exception as e:
        print(f"No issue_link found {str(e)}")
        issue_link = ''

    try:
        created_on = issue.fields.created
    except Exception as e:
        print(f"No created_on found {str(e)}")
        created_on = ''

    try:
        priority = issue.fields.priority.name
    except Exception as e:
        print(f"No priority found {str(e)}")
        priority = ''
    
    try:
        status = issue.fields.status.name
    except Exception as e:
        print(f"No status found {str(e)}")
        status = ''
    
    try:
        components = [component.name for component in issue.fields.components]
    except Exception as e:
        print(f"No components found {str(e)}")
        components = ''

    try:
        versions = issue.fields.versions[0].name
    except Exception as e:
        print(f"No versions found {str(e)}")
        versions = []
    try:
        labels = issue.fields.labels
    except Exception as e:
        print(f"No labels found {str(e)}")
        labels = []

    return {
            "issue_name": issue_name,
            "link": issue_link,
            "created_on": created_on,
            "priority": priority,
            "status": status,
            "assignee": issue_assignee,
            "component": components,
            "affects_versions": versions,
            "labels": labels
        }


def generate_jira_issues_list(jira_server, jira_obj, rhel_bug_ids_mapping_list):
    jira_issues_details_list = []
    jira_issues_details = {'hit_count': -1}
    for issue in rhel_bug_ids_mapping_list:
        issue_id = issue['rhel_issue_id']

        try:
            jira_issues_details = jira_get_issue_details(jira_server, jira_obj, issue_id)
            print(f"ISSUE issue hit_count: {issue['hit_count']}, jira_issues_details: {jira_issues_details}")

            jira_issues_details['hit_count'] = issue['hit_count']
        except:
            print(f"Can't set hit count for the issue {issue_id}")
            jira_issues_details = {'hit_count': -1}

        try:
            if jira_issues_details["status"] not in ["Closed", "Release Pending", "Verified"]:
                print(f"=== ISSUE ID: {issue_id}")
                jira_issues_details['issue_name'] = trim_issue_titles(jira_issues_details['issue_name'])
                jira_issues_details_list.append(jira_issues_details)

                # Update RHEL issue labels
                labels = update_label(jira_obj, issue_id, new_label="cki-priority")
                print("Updated labels:", issue_id, labels)
        except:
            print(f"Can't update labels for the issue {issue_id}")
            continue

    return jira_issues_details_list


def update_label(jira_obj, rhel_issue_id, new_label):
    """
    Update RHEL issue labels
    :param rhel_issue_id: rhel_issue_id
    :param new_label: new_label
    :return: list of labels
    """
    try:
        issue = jira_obj.issue(rhel_issue_id)
        labels = issue.fields.labels

        if new_label not in labels:
            # Add a new label or modify the existing ones
            labels.append(new_label)  # Add 'new_label' to the list
            # Update the issue with the new labels
            issue.update(fields={"labels": labels})
            print(colored(f"{rhel_issue_id} Updated labels: {labels}", 'red'))

            return ' '.join(labels)
        else: 
            print(f"The issue is labeled: {rhel_issue_id}, {labels}")
            return ''
    except:
        print(f"Couldn't find the issues {rhel_issue_id}")
        return ''


def remove_label(jira_obj, rhel_issue_id, label_to_remove="cki-datawarehouse"):
    """
    Update RHEL issue labels
    :param rhel_issue_id: rhel_issue_id
    :param new_label: new_label
    :return: list of labels
    """
    try:
        issue = jira_obj.issue(rhel_issue_id)
        labels = issue.fields.labels
    except:
        print(f"You do not have the permission to see the specified issue: {rhel_issue_id}")
        return ""

    try:
        labels.remove(label_to_remove)
    except:
        print(f"There is no label {label_to_remove} in the issue {rhel_issue_id}")
        return ' '.join(labels)
    # Update the issue with the new labels
    try:
        issue.update(fields={"labels": labels})
        print(f"{issue.id} Removed label: {label_to_remove} Updated labels: {labels}")
        ' '.join(labels)
    except:
        print(f"Can't remove label {label_to_remove} for the issue {issue.id}")
        return ' '.join(labels)
    return ' '.join(labels)


def update_pool_team(jira_obj, rhel_issue_id, pool_team):
    """
    Update RHEL issue pool team
    :param rhel_issue_id: rhel_issue_id
    :param new_label: pool_team
    :return: list of pool_teams
    """
    try:
        issue = jira_obj.issue(rhel_issue_id)

        existed_pool_team = issue.fields.customfield_12317259

        if existed_pool_team is None:

            # Update the issue with the new pool_teams
            issue.update(fields={"customfield_12317259": [{"value": pool_team}]})
            print(colored(f"{rhel_issue_id} Updated pool_team: {pool_team}", 'red'))

            return issue.fields.customfield_12317259
        else: 
            print(f"The issue is updated with the pool_team: {rhel_issue_id}, {pool_team}")
            return ''
    except:
        print(f"Couldn't find the issues {rhel_issue_id}")
        return ''