import os
import csv
from datetime import datetime, timezone, timedelta


def get_env_variables(env_variables):
    """
    Function to get and return the values of environment variables

    :param env_variables: list of variables
    :return: dict env_values
    """
    # Create a dictionary to hold the environment variables and their values
    env_values = {}

    # Iterate over the list of environment variables
    for var_name in env_variables:
        # Get the value of the environment variable
        value = os.getenv(var_name)

        # Check if the environment variable exists and add it to the dictionary
        if value:
            env_values[var_name] = value
            print(f'{var_name}: Retrieved')
        else:
            print(f'The {var_name} environment variable is not set.')
            # Optionally, you can set a default value or handle the missing variable appropriately
            # For example, setting a default value or None
            env_values[var_name] = None

    return env_values


def get_hit_count(issues, rhel_bug_ids):
    """
    Get issues hit count and mat it

    :param issues: all issues from DW
    :param rhel_bug_ids: list of RHEL bug IDs

    :return: mapped list
    """
    mapping_list = []

    for issue in issues:
        for rhel_issue_id in rhel_bug_ids:
            if rhel_issue_id in issue['ticket_url']:
                mapping_list.append(
                    {'rhel_issue_id': rhel_issue_id, 'hit_count': issue['hit_count']})

    # print(mapping_list)
    return mapping_list


def remove_file(file_path):
    """
    Removes the file at the specified path.

    :param file_path: Path of the file to be removed.
    """
    try:
        os.remove(file_path)
        print(f"File {file_path} successfully removed.")
    except FileNotFoundError:
        print(f"File {file_path} not found.")
    except OSError as e:
        print(f"Error: {e}")


def save_to_csv_file(issues, csv_file_path="csv_file.csv"):
    """
    Save data to csv file
    :param issues: list of issues
    :param csv_file_path: a patch
    :return:
    """
    # Current date in UTC for calculating days open
    current_date_utc = datetime.now(timezone.utc)

    # Prepare data for CSV
    csv_data = []
    for _, issue in enumerate(issues, start=1):
        try:
            created_on = datetime.strptime(issue['created_on'], '%Y-%m-%dT%H:%M:%S.%f%z')
            days_open = (current_date_utc - created_on).days
            components = ', '.join(issue['component'])  # Join components into a single string
            issue['link'] = "=HYPERLINK(" + "\"" + issue['link'] + "\"" + ", " + "\"" + issue['link'].split('/')[-1] + "\"" + ")"

        except:
            created_on = ""
            days_open = ""
            components = ""
            issue['link'] = ""
            
        try:
            issue_assignee = issue['assignee']
        except:
            issue_assignee = ''


        csv_data.append([
            issue['hit_count'],
            issue['issue_name'],
            issue['link'],
            days_open,
            issue['priority'],
            issue['status'],
            issue_assignee,
            components
        ])

    # Writing the data to a CSV file
    with open(csv_file_path, mode='w', newline='', encoding='utf-8') as file:
        writer = csv.writer(file)
        # Writing the headers
        writer.writerow(
            ["hit_count", "issue_name", "link", "days_open", "priority", "status", "assignee", "component"])
        # Writing the rows
        writer.writerows(csv_data)

    print(f"Issues data saved to {csv_file_path}")


def extract_specific_links(all_issues_list):
    """
    Get other_issues_links, bugzilla_links, issues_links from the list of issues
    :param all_issues_list: all issues
    :return: other_issues_links, bugzilla_links, issues_links
    """

    bugzilla_links = [link for link in all_issues_list if 'bugzilla.redhat.com' in link]
    issues_links = [link for link in all_issues_list if 'issues.redhat.com' in link]
    other_issues_links = [link for link in all_issues_list if 'bugzilla.redhat.com' not in link and 'issues.redhat.com' not in link]

    # print("other_issues_links", other_issues_links)
    # print("Bugzilla Links:", bugzilla_links)
    # print("Issues.redhat.com Links:", issues_links)
    # print("Other Issues:", other_issues_links)
    print("Total bugs:", len(bugzilla_links) + len(issues_links))

    return other_issues_links, bugzilla_links, issues_links


def date_days_ago(days_ago=7):
    # Get today's date
    today = datetime.today().date()

    # Calculate the date 7 days before today
    days_ago = today - timedelta(days=days_ago)

    # Return the date in "YYYY-MM-DD" format
    return days_ago.strftime("%Y-%m-%d")


def trim_issue_titles(issue_title, trim_len=60):
    """
    Trim string to the proper len

    :param issue_title: whole string
    :param trim_len: int
    :return: trimmed string
    """
    if len(issue_title) > trim_len:
        return f"{issue_title[:trim_len]}..."
    else:
        return issue_title


def get_issues_ids_from_url(list_of_issue):
    """
    Get IDs from issues URLS

    :param list_of_issue:
    :return:
    """
    gitlab_ids = []
    for link in list_of_issue:
        if "gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests" in link:
            issue_id = link.split('/')[-1]
            try:
                gitlab_ids.append(int(issue_id))
            except ValueError:
                continue
    return gitlab_ids


def days_since_issue_changed(created_at):
    """
    Calculate the number of days from the issue creation date until now.

    Args:
    - created_at (str): The creation date of the issue in ISO 8601 format (e.g., "2023-01-29T18:23:06.954Z").

    Returns:
    - int: The number of days since the issue was created until now.
    """
    created_at_date = datetime.strptime(created_at, "%Y-%m-%dT%H:%M:%S.%fZ")
    created_at_date = created_at_date.replace(tzinfo=timezone.utc)
    now = datetime.now(timezone.utc)
    days_open = (now - created_at_date).days
    return days_open


def save_dicts_to_csv(dict_list, filename):
    """
    Saves a list of dictionaries to a CSV file. Each dictionary in the list represents a row in the CSV.

    Args:
    - dict_list (list): A list of dictionaries containing the data to be saved.
    - filename (str): The name of the CSV file where the data will be saved.
    """
    # Check if the list is not empty and proceed
    if dict_list:
        # Open the file in write mode
        with open(filename, mode='w', newline='', encoding='utf-8') as file:
            # Create a CSV writer object
            writer = csv.DictWriter(file, fieldnames=dict_list[0].keys())

            # Write the header
            writer.writeheader()

            # Write the rows
            for d in dict_list:
                writer.writerow(d)