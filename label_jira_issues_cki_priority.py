import re

import pandas as pd
import os

from datetime import datetime
from termcolor import colored

from src.tools import get_env_variables, trim_issue_titles, date_days_ago, remove_file
from src.jira_methods import get_jira_object, get_jira_issue_id_from_link, jira_get_issue_details, update_label, remove_label, update_pool_team
from src.datawarehouse_methods import get_issues_raw


# List of environment variables to check, all in uppercase
env_variables = [
    'JIRA_USER',
    'JIRA_SERVER',
    'JIRA_TOKEN_AUTH',
    'DATAWAREHOUSE_ENDPOINT',
    'DATAWAREHOUSE_TOKEN'
]

# Function to flatten data
def flatten_data(data):
    return {
        "id": data["id"],
        "kind_id": data["kind"]["id"],
        "kind_description": data["kind"]["description"],
        "kind_tag": data["kind"]["tag"],
        "description": data["description"],
        "ticket_url": data["ticket_url"],
        "resolved": data["resolved"],
        "regexes": data["regexes"],
        "resolved_at": data["resolved_at"],
        "policy_id": data["policy"]["id"],
        "policy_name": data["policy"]["name"],
        "policy_read_group": data["policy"]["read_group"],
        "policy_write_group": data["policy"]["write_group"],
        "first_seen": data["first_seen"],
        "last_seen": data["last_seen"],
        "hit_count": data["hit_count"]
    }


def generate_dw_endpoint():
    seven_days_ago = date_days_ago(days_ago=7)
    datawarehouse_endpoint = f"{env_values['DATAWAREHOUSE_ENDPOINT']}/api/1/issue/top/?date_from={seven_days_ago}&limit=100"
    return datawarehouse_endpoint


def get_dw_issues_from_one_page(datawarehouse_endpoint, csv_file):
    "Tag DW issues"
    issues = get_issues_raw(endpoint=datawarehouse_endpoint, datawarehouse_token=env_values['DATAWAREHOUSE_TOKEN'])

    # Flatten all data entries
    flattened_data_entries = [flatten_data(entry) for entry in issues['results']]
    
    # Convert new data to DataFrame
    new_data_df = pd.DataFrame(flattened_data_entries)

    # Check if the CSV file exists
    if os.path.exists(csv_file):
        # Read the existing CSV file
        existing_df = pd.read_csv(csv_file)
        # Append the new data to the existing DataFrame
        updated_df = pd.concat([existing_df, new_data_df], ignore_index=True)
        
    else:
        # If the file doesn't exist, the new data becomes the DataFrame
        updated_df = new_data_df

    # Save DataFrame to CSV
    updated_df.to_csv(csv_file, index=False)

    # print(issues)

    return {'count': issues['count'], 'next': issues['next']}


def get_all_dw_issues(csv_file):

    # Remove file before adding new data
    remove_file(csv_file)

    # Get DW issues from all pages
    datawarehouse_endpoint = generate_dw_endpoint()

    # get issues from the first page
    dw_issues = get_dw_issues_from_one_page(datawarehouse_endpoint, csv_file)
    # get link for the next page and get issues from the page
    while dw_issues['next']:
         dw_issues = get_dw_issues_from_one_page(dw_issues['next'], csv_file)
         print(f"=== COUNT: {dw_issues['count']}   DW Issues: {dw_issues['next']}")


def update_all_dw_issues(in_csv_file, out_csv_file='upd_dw_bug_reports.csv'):
    # Read the existing CSV file
    df = pd.read_csv(in_csv_file)
    
    # Add dw_url colimn to show DW URL 
    df['dw_url'] = "https://datawarehouse.cki-project.org/issue/" + df['id'].astype(str)

    # Add ticket_id column and update it for the rhel issues
    df['ticket_id'] = None
    df['created_days_ago'] = None
    df['updated_days_ago'] = None
    df['assignee'] = None
    df['spreadsheet_dw_link'] = None
    df['priority'] = None
    df['status'] = None
    df['component'] = None
    df['first_seen_days_left'] = None
    df['last_seen_days_left'] = None

    # Define the pattern
    rhel_pattern = r'[A-Z]+-[0-9]+'
    gitlab_pattern = r'issues/(\d+)'

    # Extract the RHEL ticket_id where it does not already exist
    df['ticket_id'] = df.apply(
        lambda row: re.search(rhel_pattern, row['ticket_url']).group(0) 
        if pd.isna(row['ticket_id']) and re.search(rhel_pattern, row['ticket_url']) 
        else row['ticket_id'], axis=1)

    # Update the gitlab ticket_ids where they do not already exist
    df['ticket_id'] = df.apply(
        lambda row: re.search(gitlab_pattern, row['ticket_url']).group(1) 
        if pd.isna(row['ticket_id']) and re.search(gitlab_pattern, row['ticket_url']) 
        else row['ticket_id'], axis=1)
    
    # Update the 'spreadsheet_dw_link' column
    df['spreadsheet_dw_link'] = df.apply(
        lambda row: f'=HYPERLINK("{row["dw_url"]}", "{row["hit_count"]}")', axis=1)
    
    # Replace double quotes with single quotes in the 'description' column
    df['description'] = df['description'].str.replace('"', "'")
    
    # Apply the trim_issue_titles function to each row in the 'description' column
    df['description'] = df['description'].apply(trim_issue_titles)

    # Update the 'title' column
    df['title'] = df.apply(
        lambda row: f'=HYPERLINK("{row["ticket_url"]}", "{row["description"]}")', axis=1)
    
    # Convert string to datetime
    df['first_seen'] = pd.to_datetime(df['first_seen'], errors='coerce', utc=True).dt.tz_localize(None)
    df['last_seen'] = pd.to_datetime(df['last_seen'], errors='coerce', utc=True).dt.tz_localize(None)
    # Convert current date to tz-naive
    today = pd.Timestamp(datetime.utcnow()).tz_localize(None)
    # Calculate days left
    df['first_seen_days_left'] = (df['first_seen'] - today).dt.days
    df['last_seen_days_left'] = (df['last_seen'] - today).dt.days
    # Removing the minus sign by taking the absolute value
    df['first_seen_days_left'] = df['first_seen_days_left'].abs()
    df['last_seen_days_left'] = df['last_seen_days_left'].abs()

    df.to_csv(out_csv_file, index=False)
    return out_csv_file


def get_jira_issue_details(jira_obj, issues_url):
    # Gets Jira issues details name, version, labels, status, etc
    jira_issue_id = get_jira_issue_id_from_link(issues_url)
    jira_issues_details = jira_get_issue_details(env_values['JIRA_SERVER'], jira_obj, jira_issue_id)

    return jira_issues_details


def get_jira_issues_with_label(jira_obj, label):
    # JQL query to find issues with the label "ski-datawarehouse"
    jql_str = f'labels = {label}'

    # Search for issues
    issues = jira_obj.search_issues(jql_str, maxResults=500)
    print(len(issues))
    return issues


def remove_issue_label(jira_obj, issues, label_to_remove):
    # Remove label from Jira issues if issue is closed
    for issue in issues:
        try:
            status = issue.fields.status.name
        except Exception as e:
            print(f"No status found {str(e)}")
            status = ''
        # if status == "Closed":
        remove_label(jira_obj, rhel_issue_id=issue.id, label_to_remove=label_to_remove)


def find_labeled_issue_and_remove_label(jira_obj, label):
    # Remove labels for closed issues cki-datawarehouse cki-priority
    jira_issues_with_label = get_jira_issues_with_label(jira_obj, label=label)
    remove_issue_label(jira_obj, issues=jira_issues_with_label, label_to_remove=label)


def get_unresolved_ticket_urls(csv_file):
    """
    Reads a CSV file, filters the DataFrame to include only unresolved issues
    with ticket URLs containing 'https://issues.redhat.com', and returns a list of ticket URLs.

    Args:
        csv_file (str): The path to the CSV file.

    Returns:
        list: A list of ticket URLs that match the criteria.
    """
    # Read the existing CSV file
    df = pd.read_csv(csv_file)
    
    # Filter the DataFrame based on the conditions
    filtered_df = df[(df['resolved'] == False) & (df['ticket_url'].str.contains("https://issues.redhat.com"))]
    
    # Return the list of ticket URLs
    return filtered_df['ticket_url'].tolist()


def get_jira_issue_ids_from_urls(jira_issue_list):

    jira_issue_ids = []
    # Regex pattern to match "RHELPLAN-117194"
    pattern = r'[A-Z]+-[0-9]+'

    for url in jira_issue_list:
        # Find all matches in the URL
        matches = re.findall(pattern, url)
        jira_issue_ids.append(matches[0])
    return jira_issue_ids


def update_labels_for_all_opened_cki_dw_issues(csv_file, new_label="cki-datawarehouse"):

    unresolved_ticket_urls = get_unresolved_ticket_urls(csv_file=csv_file)
    jira_issue_ids = get_jira_issue_ids_from_urls(unresolved_ticket_urls)

    for issues_id in jira_issue_ids:
        update_label(jira_obj, rhel_issue_id=issues_id, new_label=new_label)


def update_labels_for_high_priority_issues(csv_file, old_labeled_items = []):

    # Read the existing CSV file
    df = pd.read_csv(csv_file)
    filtered_df = df[(df['resolved'] == False) & (df['hit_count'] >= 10) & (df['ticket_url'].str.contains("https://issues.redhat.com"))]

    issues_ids = filtered_df['ticket_url'].tolist()
    jira_issue_ids = get_jira_issue_ids_from_urls(issues_ids)
    print(f"LAST WEEK ISSUES: {old_labeled_items}")
    print(f"TOP HIGH PRIORITY ISSUES: {jira_issue_ids}")

    for issues_id in jira_issue_ids:
        remove_label(jira_obj, rhel_issue_id=issues_id, label_to_remove="cki-priority:low")
        remove_label(jira_obj, rhel_issue_id=issues_id, label_to_remove="cki-priority:medium")
        update_label(jira_obj, rhel_issue_id=issues_id, new_label="cki-priority:high")


def update_labels_for_medium_priority_issues(csv_file, old_labeled_items = []):

    # Read the existing CSV file
    df = pd.read_csv(csv_file)
    filtered_df = df[(df['resolved'] == False) & (df['hit_count'] >= 2) & (df['hit_count'] <= 9) & (df['ticket_url'].str.contains("https://issues.redhat.com"))]

    issues_ids = filtered_df['ticket_url'].tolist()
    jira_issue_ids = get_jira_issue_ids_from_urls(issues_ids)
    print(f"TOP MEDIUM PRIORITY ISSUES: {jira_issue_ids}")

    for issues_id in jira_issue_ids:
        remove_label(jira_obj, rhel_issue_id=issues_id, label_to_remove="cki-priority:low")
        remove_label(jira_obj, rhel_issue_id=issues_id, label_to_remove="cki-priority:high")
        update_label(jira_obj, rhel_issue_id=issues_id, new_label="cki-priority:medium")


def update_labels_for_low_priority_issues(csv_file, old_labeled_items = []):

    # Read the existing CSV file
    df = pd.read_csv(csv_file)
    filtered_df = df[(df['resolved'] == False) & (df['hit_count'] == 1) & (df['ticket_url'].str.contains("https://issues.redhat.com"))]

    issues_ids = filtered_df['ticket_url'].tolist()
    jira_issue_ids = get_jira_issue_ids_from_urls(issues_ids)
    print(f"TOP LOW PRIORITY ISSUES: {jira_issue_ids}")

    for issues_id in jira_issue_ids:
        remove_label(jira_obj, rhel_issue_id=issues_id, label_to_remove="cki-priority:medium")
        remove_label(jira_obj, rhel_issue_id=issues_id, label_to_remove="cki-priority:high")
        update_label(jira_obj, rhel_issue_id=issues_id, new_label="cki-priority:low")


def get_first_ten_issues_with_max_hit_count(csv_file, pattern, file_to_save="top_rhel_issues.csv", issues_number=10):
    """
    csv_file: the file with the data
    pattern: string that contains URL info. For example:
        https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests 
        https://issues.redhat.com
    """
    # Read the existing CSV file
    df = pd.read_csv(csv_file)

    # Filter the DataFrame with the corrected condition
    filtered_df = df[
        df['status'].isin(["New", "Planning", "In Progress", ""]) &  # Status should be one of these
        df['ticket_url'].str.contains(pattern) &  # Matches the pattern
        ~df['ticket_url'].str.contains("RHELTEST")  # Excludes "RHELTEST"
    ]

    sorted_filtered_df = filtered_df.sort_values(by='hit_count', ascending=False).head(issues_number)

    sorted_filtered_df['hit_count'] = sorted_filtered_df['spreadsheet_dw_link']

    # Reorder the columns
    new_order = [
        'hit_count', 'title', 'assignee',
        'priority', 'status', 'component', 'ticket_url'
    ]

    rhel_first_ten_issues = sorted_filtered_df.reindex(columns=new_order)
    rhel_first_ten_issues.to_csv(file_to_save, index=False)

    return rhel_first_ten_issues


def get_rhel_ten_beta_issues_with_max_hit_count(csv_file, pattern, issues_number=10):
    """
    csv_file: the file with the data
    pattern: string that contains URL info. For example:
        https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests 
        https://issues.redhat.com
    """
    # Read the existing CSV file
    df = pd.read_csv(csv_file)

    filtered_df = df[(df['status'] != "Closed") & (df['status'] != "Release Pending") & (df['affects_versions'] == "rhel-10.0.beta") & df['ticket_url'].str.contains(pattern)]
    sorted_filtered_df = filtered_df.sort_values(by='hit_count', ascending=False).head(issues_number)

    sorted_filtered_df['hit_count'] = sorted_filtered_df['spreadsheet_dw_link']

    # Reorder the columns
    new_order = [
        'hit_count', 'title', 'assignee',
        'priority', 'status', 'component', 'affects_versions'
    ]

    rhel_first_ten_issues = sorted_filtered_df.reindex(columns=new_order)
    rhel_first_ten_issues.to_csv("top_rhel_ten_beta_issues.csv", index=False)

    return


def get_ustable_test_issues(csv_file, pattern, issues_number=10):
    """
    csv_file: the file with the data
    pattern: string that contains URL info. For example:
        https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests 
        https://issues.redhat.com
    """
    # Read the existing CSV file
    df = pd.read_csv(csv_file)

    filtered_df = df[(df['status'] != "Closed") & (df['status'] != "Release Pending") & df['ticket_url'].str.contains(pattern)]
    sorted_filtered_df = filtered_df.sort_values(by='hit_count', ascending=False).head(issues_number)

    sorted_filtered_df['hit_count'] = sorted_filtered_df['spreadsheet_dw_link']

    # Reorder the columns
    new_order = [
        'hit_count', 'title', 'assignee',
        'priority', 'status', 'component', 'affects_versions'
    ]

    rhel_first_ten_issues = sorted_filtered_df.reindex(columns=new_order)
    rhel_first_ten_issues.to_csv("ustable_test_issues.csv", index=False)

    return


def get_real_hrel_issue_status(csv_file, pattern):
    """
    csv_file: the file with the data
    pattern: string that contains URL info. For example:
        https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests 
        https://issues.redhat.com
    """
    # Read the existing CSV file
    df = pd.read_csv(csv_file)

    # Ensure that the DataFrame has 'affects_versions' and 'labels' columns
    if 'affects_versions' not in df.columns:
        df['affects_versions'] = ""
    if 'labels' not in df.columns:
        df['labels'] = ""

    # Filter the DataFrame for unresolved tickets that match the pattern
    filtered_df = df[(df['resolved'] != "Closed") & (df['resolved'] != "Release Pending") & df['ticket_url'].str.contains(pattern)]

    # Define a function to apply the get_real_status and extract the values into new columns
    def apply_get_real_status(row):
        real_status = get_jira_issue_details(jira_obj, row['ticket_url'])
        try:
            # row['affects_versions'] = ', '.join(real_status['affects_versions'])
            row['affects_versions'] = real_status['affects_versions']
        except:
            print(colored("can't get issue affects_versions", 'red'))
            row['affects_versions'] = ""
        try:
            row['status'] = real_status['status']
        except:
            print(colored("can't get issue status", 'red'))
            row['status'] = ""
        try:
            row['priority'] = real_status['priority']
        except:
            print(colored("can't get issue priority", 'red'))
            row['priority'] = ""
        try:
            row['assignee'] = real_status['assignee']
        except:
            print(colored("can't get issue assignee", 'red'))
            row['assignee'] = ""
        try:
            row['component'] = real_status['component']
        except:
            print(colored("can't get issue component", 'red'))
            row['component'] = ""
        try:
            row['labels'] = ', '.join(real_status['labels'])
        except:
            print(colored("can't get issue labels", 'red'))
            row['labels'] = ""
        
        return row
    
    # Apply the function to each row in the filtered DataFrame
    updated_filtered_df = filtered_df.apply(apply_get_real_status, axis=1)

    # Update the original DataFrame with the new data
    df.update(updated_filtered_df)

    # Save the DataFrame back to the original CSV file
    df.to_csv(csv_file, index=False)
    # df.to_csv("real_rhel_status.csv", index=False)
    return


def get_sst_teams_ids(input_sst_file="all_ssts.csv", file_to_update="upd_dw_bug_reports.csv"):
    # Load the CSV files
    all_ssts = pd.read_csv(input_sst_file)
    upd_dw_bug_reports = pd.read_csv(file_to_update)
    # Select relevant columns from all_ssts
    ssts_filtered = all_ssts[['Krb ID', 'Name', 'Component', 'Team']]

    # Create a new column 'sst_team' with only the first team name
    ssts_filtered['sst_team'] = ssts_filtered['Team'].apply(lambda x: x.split('·')[0] if pd.notna(x) else x)

    # Drop the original 'team' column as it's not needed for merging
    ssts_filtered = ssts_filtered.drop(columns=['Team'])

    # Merge the files based on 'Name' from all_ssts and 'assignee' from upd_dw_bug_reports
    upd_dw_bug_reports = upd_dw_bug_reports.merge(ssts_filtered, left_on='assignee', right_on='Name', how='left')

    # Save the updated DataFrame back to the same file
    upd_dw_bug_reports.to_csv(file_to_update, index=False)

    print("Update completed. The file 'upd_dw_bug_reports.csv' has been updated with the new 'sst_team' column.")


def main_function(csv_file='dw_bug_reports.csv', get_hrel_issue_status=True,
                  get_all_issues=True, get_sst_teams=True,
                  label_cki_dw_issues=True, label_priority_issues=True,
                  generate_rhel_ten_beta_report=True,
                  generate_unstable_tests_report=True, generate_top_ten_issues_report=True):

    # Gettining all the issues from the DW (takes too long)
    if get_all_issues:
        print(colored('Gettining all the issues from the DW (takes too long)', 'green'))
        get_all_dw_issues(csv_file)
        csv_file = update_all_dw_issues(csv_file)

    if get_hrel_issue_status:
        get_real_hrel_issue_status(csv_file, pattern="https://issues.redhat.com")
    
    if get_sst_teams:
        get_sst_teams_ids(input_sst_file="all_ssts.csv", file_to_update="upd_dw_bug_reports.csv")

    if label_cki_dw_issues:
        print(colored('find_labeled_issue_and_remove_label', 'green'))
        ### find_labeled_issue_and_remove_label(jira_obj, label="cki-datawarehouse")
        print(colored('update_labels_for_all_opened_cki_dw_issues', 'green'))
        update_labels_for_all_opened_cki_dw_issues(csv_file)

    if label_priority_issues:
        print(colored('update_labels for HIGH priority', 'green'))
        update_labels_for_high_priority_issues(csv_file)
        print(colored('update_labels for MEDIUM priority', 'green'))
        update_labels_for_medium_priority_issues(csv_file)
        print(colored('update_labels for LOW priority', 'green'))
        update_labels_for_low_priority_issues(csv_file)
    
    if generate_unstable_tests_report:
        print(colored('update_df_with_gitlab_issue_details', 'green'))
        get_ustable_test_issues(csv_file, pattern="RHELTEST")
        # update_df_with_gitlab_issue_details(gitlab_first_ten_issues)

    if generate_top_ten_issues_report:
        print(colored('update_df_with_rhel_issue_details', 'green'))
        get_first_ten_issues_with_max_hit_count(csv_file, pattern="https://issues.redhat.com", issues_number=10)
    
    if generate_rhel_ten_beta_report:
        print(colored('get_rhel_ten_beta_issues_with_max_hit_count', 'green'))
        get_rhel_ten_beta_issues_with_max_hit_count(csv_file, pattern="https://issues.redhat.com", issues_number=10)


if __name__ == '__main__':
    # Get the environment variables and their values
    print(colored('Get the environment variables and their values', 'green'))
    env_values = get_env_variables(env_variables)
    jira_obj = get_jira_object(env_values['JIRA_SERVER'], env_values['JIRA_TOKEN_AUTH'])

    main_function(csv_file="upd_dw_bug_reports.csv", 
                  get_all_issues=False,
                  get_hrel_issue_status=False, 
                  get_sst_teams=False, # ##
                  label_cki_dw_issues=False,
                  label_priority_issues=True,
                  generate_rhel_ten_beta_report=True,
                  generate_unstable_tests_report=True,
                  generate_top_ten_issues_report=True)


    def get_unresolved_tickets(file_path="upd_dw_bug_reports.csv"):
        # Read the CSV file
        df = pd.read_csv(file_path)
        
        # Filter rows where "resolved" is "FALSE" and "sst_team" is not empty
        filtered_df = df[(df["resolved"] == False) & (df["sst_team"].notna()) & (df["sst_team"] != "")]
        # Select relevant columns
        selected_data = filtered_df[["ticket_id", "sst_team", "assignee"]]
        # Convert to list of dictionaries
        result_list = selected_data.to_dict(orient="records")

        for item in result_list:
            update_pool_team(jira_obj, rhel_issue_id=item["ticket_id"], pool_team=item["sst_team"])
        
        return result_list

    # Example usage
    tickets = get_unresolved_tickets()
    print(len(tickets))




# item_data = [
#     {"ticket_id": "RHELTEST-155", "sst_team": "platmgmt-ins-hbi", "assignee": "Andrew Price"},
#     {"ticket_id": "RHELTEST-677", "sst_team": "rhel-sst-logical-storage", "assignee": "Xiao Ni"},
#     {"ticket_id": "RHELTEST-69", "sst_team": "rhel-sst-kernel-debug", "assignee": "Xiaoying Yan"},
#     {"ticket_id": "RHELTEST-659", "sst_team": "rhel-sst-kernel-maintainers", "assignee": "Zhijun Wang"},
#     {"ticket_id": "RHELTEST-230", "sst_team": "rhel-sst-kernel-maintainers", "assignee": "Zhijun Wang"},
# ]

# for item in item_data:
#     output = update_pool_team(jira_obj, rhel_issue_id=item["ticket_id"], pool_team=item["sst_team"])
